###############################################################
# Jau-er Chen and Yu-Chang Chen, August 2019. ver.2.
# following Kim and Oka (2012), Journal of Applied Econometrics 
###############################################################

# The algorithm is basically the iteration between SSR1 and SSR2
# SSR1 is the minimization of  beta coefficient 
# SSR2 is the minimization of factor structure
# Two orders are considered: SSR1 then SSR2, SSR2 then SSR1
# In the final step(step3) pick the coefficient where SSR is smaller

# This program applies to Kim and Oka(2012) only

# The instruction in Kim and Oka(2012) 
# Step 1: Obtain the least squares estimates based on SSR1, ignoring the common factor structure.
# 	Given the coefficient estimates, subsequently estimate the factor structure by minimizing
# 	SSR2 and continue to iterate between SSR1 and SSR2 until the percent change in the
# 	SSR meets a certain threshold value.

# Step 2: Estimate the factor structure based on SSR2, assuming that all regression coefficients are
# 	zero. Given the estimated common factor structure, subsequently estimate the regression
# 	coefficients by minimizing SSR1 and continue to iterate as in step 1.

# Step 3: Pick up the set of estimates giving the smaller SSR from the two sets of estimates obtained in
# 	steps 1 and 2.

# I have to import some function from phtt:
# fpca.fit: used to calcuate factor structure
# The threshold mentioned in Kim and Oka is 10^-9, but could be very time-consuming
# Even 10^-6 takes several minutes, convergence in step2 is very slow. 
# Step1 takes about 1000 and step2 takes 3700
# If we don't specify no. of factors

rm(list=ls(all=TRUE))
# set working directory
setwd("/Users/jauer/Dropbox/talks_prepared/workshop2019_factor/prg/kimoka")

#Load package and data
library(foreign)
library(phtt)
data <- read.dta("Kim and Oka -Specific time trend.dta")
T<-33
N<-48
d.l.r_div_p1000 <- matrix(data$r_div_p1000, T,N)

#transforming data into matrix form, as requset of Eup()
d.l.d1<-matrix(data$d1, T,N)
d.l.d3<-matrix(data$d3, T,N)
d.l.d5<-matrix(data$d5, T,N)
d.l.d7<-matrix(data$d7, T,N)
d.l.d9<-matrix(data$d9, T,N)
d.l.d11<-matrix(data$d11, T,N)
d.l.d13<-matrix(data$d13, T,N)
d.l.d15<-matrix(data$d15, T,N)
d.l.r_div_p1000<-matrix(data$r_div_p1000, T,N)
d.l.log_div<-matrix(data$log_div, T,N)


#####################################################################
threshold<- 10^-9
#####################################################################

# number of factors = r 
r <- 9
#####################################################################


# Step 1:SSR1 then SSR2

# Estimate beta coefficient with no factor structure
# No time trend as the specification of t3col4
t3col4 <- Eup(d.l.r_div_p1000~d.l.d1+d.l.d3+d.l.d5+d.l.d7+d.l.d9+d.l.d11+d.l.d13+d.l.d15-1,additive.effects=c("twoways"),factor.dim=0) 
y2_raw <- d.l.r_div_p1000-t3col4$fitted.values

# pca analysis
# Follow notation in Diffusion Index(2002)

X <- y2_raw
# Picking the eigenvectors with respect to  largest eigenvalues
# This could be easily because of the design of function eigen
lamda <- N^(1/2)*(eigen(t(X)%*%X)$vectors[,c(seq(1,r))])
F <- X%*%lamda/N
X_hat <- F%*%t(lamda)
SSR <- sum((d.l.r_div_p1000-X_hat-t3col4$fitted.values)^2)

# Starting iteration
# y1, y2 is the matrix form of y1_st and y2_st as in the paper
counter<-0
repeat{
	y1<-d.l.r_div_p1000-X_hat
	t3col4<-Eup(y1~d.l.d1+d.l.d3+d.l.d5+d.l.d7+d.l.d9+d.l.d11+d.l.d13+d.l.d15-1,additive.effects=c("twoways"),factor.dim=0) 
	
	y2_raw<-d.l.r_div_p1000-t3col4$fitted.values

	#pca analysis
	X<-y2_raw
	lamda<-N^(1/2)*(eigen(t(X)%*%X)$vectors[,c(seq(1,r))])
	F<-X%*%lamda/N
	X_hat<-F%*%t(lamda)


	SSR_prime<-sum((d.l.r_div_p1000-X_hat-t3col4$fitted.values)^2)
	#Update status
	counter<-counter+1
	print(paste("Now at iteration:",counter))
	print(paste("SSR=",SSR,"SSR'=",SSR_prime,"percentage change:",(SSR_prime-SSR)/SSR*100))
	print(counter)
	flush.console()  
	
	#Checking for convergence: stop iteration if (SSR'-SSR)/SSR <threshold
	if( abs((SSR_prime-SSR))/SSR*100<threshold){
		print("done!")
		print(counter)
		break
	}
	SSR<-SSR_prime
}
step1_SSR<-SSR
step1_estimate<-t3col4

#####################################################################
# Step 2:SSR2 then SSR1
# Estimate factor structure assuming beta coefficients all zero
# No time trend as the specification of t3col4
y2_raw<-d.l.r_div_p1000


X<-y2_raw
# Picking the eigenvectors with respect to  largest eigenvalues
# This could be easily done because of the design of function eigen
lamda<-N^(1/2)*(eigen(t(X)%*%X)$vectors[,c(seq(1,r))])
F<-X%*%lamda/N
X_hat<-F%*%t(lamda)
SSR<-sum((d.l.r_div_p1000-X_hat-t3col4$fitted.values)^2)

#Estimate beta coefficients

y1<-d.l.r_div_p1000-X_hat
t3col4<-Eup(y1~d.l.d1+d.l.d3+d.l.d5+d.l.d7+d.l.d9+d.l.d11+d.l.d13+d.l.d15-1,additive.effects=c("twoways"),factor.dim=0) 

SSR<-sum((d.l.r_div_p1000-X_hat-t3col4$fitted.values)^2)

# Starting iteration
# y1, y2 is the matrix form of y1_st and y2_st as in the paper
counter<-0
repeat{
	#pca analysis
	y2_raw<-d.l.r_div_p1000-t3col4$fitted.values

	X<-y2_raw
	# Picking the eigenvectors with respect to  largest eigenvalues
	# This could be easily done because of the design of function eigen
	lamda<-N^(1/2)*(eigen(t(X)%*%X)$vectors[,c(seq(1,r))])
	F<-X%*%lamda/N
	X_hat<-F%*%t(lamda)

	y1<-d.l.r_div_p1000-X_hat
	t3col4<-Eup(y1~d.l.d1+d.l.d3+d.l.d5+d.l.d7+d.l.d9+d.l.d11+d.l.d13+d.l.d15-1,additive.effects=c("twoways"),factor.dim=0) 

	SSR_prime<-sum((d.l.r_div_p1000-X_hat-t3col4$fitted.values)^2)
	#Update status
	counter<-counter+1
	print(paste("Now at iteration:",counter))
	print(paste("SSR=",SSR,"SSR'=",SSR_prime,"percentage change:",(SSR_prime-SSR)/SSR*100))
	print(counter)
	flush.console()  
	
	#Checking for convergence: stop iteration if (SSR'-SSR)/SSR <threshold
	if( abs((SSR_prime-SSR))/SSR*100<threshold){
		print("done!")
		print(counter)
		break
	}
	SSR<-SSR_prime
}
step2_SSR<-SSR
step2_estimate<-t3col4


# Step 3:Choosing the estimates in step 1&2 which results in smaller SSR

if (step1_SSR<step2_SSR){
	t3col4<-step1_estimate
	print ("step1 estimates is chosen")
} else{
	t3col4<-step2_estimate
	print ("step2 estimates is chosen")

}

# end of Kim and Oka's algorithm
# ----------------------------------------------------------------

print(t3col4)
# Results, SSR1 SSR2, Kim and Oka's algorithm, 
# without specifying explicitly linear trend and quandratic trend
# (Intercept) d.l.d1 d.l.d3 d.l.d5 d.l.d7 d.l.d9 d.l.d11  d.l.d13 d.l.d15
#             0.0828  0.233  0.209  0.191 0.0491  0.0497 -0.00139  0.0914




# ----------------------------------------------------------------
# Compared to Eup(): estimates from the phtt package
# without specifying explicitly linear trend and quandratic trend
t3col4_Eup<-Eup(d.l.r_div_p1000~d.l.d1+d.l.d3+d.l.d5+d.l.d7+d.l.d9+d.l.d11+d.l.d13+d.l.d15-1,additive.effects=c("twoways"),factor.dim=r,max.iteration=2000) 
print(t3col4_Eup)
# Results, Eup()
# (Intercept) d.l.d1 d.l.d3 d.l.d5 d.l.d7 d.l.d9 d.l.d11 d.l.d13 d.l.d15
#             0.0874  0.242  0.222  0.209 0.0711  0.0741  0.0255  0.1208

# Results in Kim and Oka (2009), t3col6, quadratic trends, ols.
# (Intercept) d.l.d1 d.l.d3 d.l.d5 d.l.d7 d.l.d9 d.l.d11 d.l.d13 d.l.d15
#             0.082  0.228  0.183  0.177  0.056  0.035   -0.001   0.125
