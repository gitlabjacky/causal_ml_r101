data <- read.dta("Kim and Oka -Specific time trend.dta")

T <- 33
N <- 48

d1 <- matrix(data$d1, T,N)
d3 <- matrix(data$d3, T,N)
d5 <- matrix(data$d5, T,N)
d7 <- matrix(data$d7, T,N)
d9 <- matrix(data$d9, T,N)
d11 <- matrix(data$d11, T,N)
d13 <- matrix(data$d13, T,N)
d15 <- matrix(data$d15, T,N)
year <- matrix(data$year, T, N)

r_div_p1000 <- matrix(data$r_div_p1000, T,N)
log_div <- matrix(data$log_div, T,N)