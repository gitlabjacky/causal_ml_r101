---
title: "Model Selection Using FDR and Lasso"
author: "b08303033 經濟四 葉秀軒"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### brief introduction
- Codebook for semi_conductor quality-control data from a semi-conductor manufacturing process. There are hundreds of diagnostic sensors along the production line, measuring various inputs and outputs in the process.
- The dataset contains the following variables:
- FAIL : a binary variable on whether the chip was a failure (failure: 1; otherwise 0)
- SIG1, SIG2, … , SIG200 : 200 vector of diagnostic signals.


### basic setup
```{r message=FALSE}
rm(list=ls(all=TRUE))
setwd("/Users/jacky/Documents/2022_08_shared/causal_ML_R101/model_selection_using_fdr_lasso/")

library(tidyverse)
library(glmnet)
library(caret)

data1 <- read_csv("semi_conductor.csv")
head(data1 )
```

### (a) logistic regression with all predictors (regressors, covariates)
```{r warning=FALSE}
data_a = data1
modela <- glm(FAIL ~ ., data=data_a, family=binomial)
summ_modela <- summary(modela)
summ_modela
```

### (b) reduce the dimension of predictors by using FDR (false discovery rate) control
```{r}
pvals <- summ_modela$coef[-c(1), 4]
q <- 0.1
  
pvals <- sort(pvals[!is.na(pvals)])
N <- length(pvals)
k <- rank(pvals, ties.method="min")
alpha <- max(pvals[ pvals<= (q*k/N) ])
  
plot(pvals, log="xy", xlab="order", main=sprintf("FDR of %g",q),
     ylab="p-value", bty="n", col=c(8,2)[(pvals<=alpha) + 1], pch=20)
  #log="xy": both axes are to be logarithmic
  #bty="n"": no box
lines(1:N, q*(1:N)/(N+1))
```
```{r}
signif <- which(pvals <= alpha)
data_b = data1[, c("FAIL", names(signif))]
modelb <- glm(FAIL ~ ., data=data_b, family=binomial)
summary(modelb)
```

### (c) estimate a penalized logistic regression (logit-lasso)
```{r}
x <- as.matrix(data1[, -c(1)])
y <- as.matrix(data1[, 1])
set.seed(4321)
lambda_test <- cv.glmnet(x, y, family="binomial", alpha=1, nfolds=5)
plot(lambda_test)
```

```{r}
best_lambda <- lambda_test$lambda.min
logit_lasso <- glmnet(x, y, family="binomial", alpha=1, lambda=best_lambda, intercept=FALSE)
lasso_coef <- predict(logit_lasso, type="coefficients", s=best_lambda)

threshold <- 0
data_c = data1[, c(1, which(abs(lasso_coef) > threshold))]
modelc <- glm(FAIL ~ ., data=data_c, family=binomial)
summary(modelc)
```

### (d) Compare the R-squared from parts (a), (b), and (c)
```{r}
adjusted_r_square <- function(model, n=nrow(data1)){
    p = length(model$coef)
    return(with(summary(model), 1-(n/(n-p))*(deviance/null.deviance)))
}

ars_a <- adjusted_r_square(modela)
ars_b <- adjusted_r_square(modelb)
ars_c <- adjusted_r_square(modelc)

data.frame(model_a=ars_a, model_b=ars_b, model_c=ars_c, row.names="adjusted R-squared")
```

### (e) evaluate the out-of-sample performance of each model through k-fold cross valuation
```{r analysis, warning=FALSE}
set.seed(4321)
RMSE <- function(data, number){
  ctrl <- trainControl(method="cv", number=number)
  cv <- train(FAIL ~ ., data=data, family=binomial, method="glm", trControl=ctrl)
  return(mean(cv$resample[, 1]))
}

RMSE_a <- RMSE(data_a, 10)
RMSE_b <- RMSE(data_b, 10)
RMSE_c <- RMSE(data_c, 10)

data.frame(model_a=c(ars_a, RMSE_a), model_b=c(ars_b, RMSE_b), model_c=c(ars_c, RMSE_c), row.names=c("adjusted R-squared", "RMSE"))
```  
- model_c does best in predicting unseen data

### code reference
- [more information](https://gitlab.com/gitlabjacky/causal_ml_r101/-/tree/main/model_selection_using_fdr_lasso)